import requests
import sys

if len (sys.argv) > 1 and sys.argv[1] == "local":
      url = "http://localhost:8080/sbn"
else:
      url = "http://draw.grew.fr/sbn"

sbn = """
female.n.02 Name "Anna Politkovskaya" % Anna Politkovskaya [0-18]
time.n.08   TPR now                   % was                [19-22]
murder.v.01 Patient -2 Time -1        % murdered.          [23-32]
"""

data = { "sbn_string": sbn }

response = requests.request("POST", url, data=data)

print(response.text)
