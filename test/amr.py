import requests
import sys

if len (sys.argv) > 1 and sys.argv[1] == "local":
      url = "http://localhost:8080/amr"
else:
      url = "http://draw.grew.fr/amr"

penman = """
(g / grow-03
      :ARG0 (f / flower)
      :ARG1 (t / thorn)
      :duration (m / multiple
            :op1 (t2 / temporal-quantity :quant 1000000
                  :unit (y / year))))
"""

data = { "penman": penman }

response = requests.request("POST", url, data=data)

print(response.text)
