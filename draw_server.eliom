open Eliom_lib
open Eliom_content
open Html.D
open Draw_server_utils

open Draw_server_main



module Draw_server_app =
  Eliom_registration.App (
  struct
    let application_name = "draw_server"
    let global_data_path = None
  end)

let main_service =
  Eliom_service.create
    ~path:(Eliom_service.Path [])
    ~meth:(Eliom_service.Get Eliom_parameter.unit)
    ()

let () =
  Draw_server_app.register
    ~service:main_service
    (fun () () ->
       Lwt.return
         (Eliom_tools.F.html
            ~title:"draw_server"
            ~css:[["css";"draw_server.css"]]
            Html.F.(body [
              h1 [txt "Welcome from draw_server!"];
            ])))

(* -------------------------------------------------------------------------------- *)
(* ping service *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["ping"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.unit
      ))
    (fun () () ->
       Log.info "<ping>";
       Lwt.return ("", "text/plain")
    )


(* -------------------------------------------------------------------------------- *)
(* amr *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["amr"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "penman")
      ))
    (fun () penman ->
       let start = Unix.gettimeofday () in
       let json = wrap amr penman in
       Log.info "{%gms} <amr> penman=[%s] ==> %s" (ms_from start) penman (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* sbn *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["sbn"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "sbn_string")
      ))
    (fun () sbn_string ->
       let start = Unix.gettimeofday () in
       let json = wrap sbn sbn_string in
       Log.info "{%gms} <sbn> sbn_string=[%s] ==> %s" (ms_from start) sbn_string (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )
