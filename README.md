
# draw.grew.fr

This repository contains the code for the web service `draw.grew.fr` which turn semantics data into SVG pictures.

The version supports AMR with the Penman notation and DRS in SBN notations use in the Parallel Meaning Bank project.

## AMR

The URL for the AMR service is http://draw.grew.fr/amr and it expects a POST request with a dict data containing a `penman` key with a string representation of the AMR, following penman notation.

In returns either:

 * a dict `{ "status": "OK", "data": "http://draw.grew.fr/123.svg" }` if it succeeds building a picture, the `data` contains the URL of the picture
 * a dict `{ "status": "ERROR", "message": "msg" }` if it fails

It should not reply:
 * a dict `{ "status": "UNEXPECTED_EXCEPTION", "exception": "…" }`. But if this happends, thanks to report it!

### Example

The following Python code builds a request and sends it to the service.

```python
import requests

url = "http://draw.grew.fr/amr"

penman = """
(g / grow-03
      :ARG0 (f / flower)
      :ARG1 (t / thorn)
      :duration (m / multiple
            :op1 (t2 / temporal-quantity :quant 1000000
                  :unit (y / year))))
"""

data = { "penman": penman }

response = requests.request("POST", url, data=data)

print(response.text)
```

## SBN

The URL for the SBN service is http://draw.grew.fr/sbn and it expects a POST request with a dict data containing a `sbn_string` key.

In returns either:

 * a dict `{ "status": "OK", "data": "http://draw.grew.fr/123.svg" }` if it succeeds building a picture, the `data` contains the URL of the picture
 * a dict `{ "status": "ERROR", "message": "msg" }` if it fails

It should not reply:
 * a dict `{ "status": "UNEXPECTED_EXCEPTION", "exception": "…" }`. But if this happends, thanks to report it!


### Example

The following Python code builds a request and sends it to the service.

```python
import requests

url = "http://draw.grew.fr/sbn"

sbn_string = """female.n.02 Name "Anna Politkovskaya" % Anna Politkovskaya [0-18]
time.n.08   TPR now                   % was                [19-22]
murder.v.01 Patient -2 Time -1        % murdered.          [23-32]
"""

data = { "sbn_string": sbn_string }

response = requests.request("POST", url, data=data)

print(response.text)
```

