open Printf
open Amr
open Conll
open Grewlib

module String_map = Map.Make (String)

exception Error of string


let status json = 
  match json |> Yojson.Basic.Util.member "status" |> Yojson.Basic.Util.to_string with
  | "OK" -> "OK"
  | _ -> Yojson.Basic.pretty_to_string json
  
let ms_from start = (Unix.gettimeofday () -. start) *. 1000.

let uid () = Unix.gettimeofday () *. 10000. |> int_of_float |> string_of_int
let config = Conll_config.build "basic"

let (warnings: Yojson.Basic.t list ref) = ref []
let warn s = warnings := (`String s) :: !warnings

let wrap fct last_arg =
  warnings := [];
  let json =
    try
      let data = fct last_arg in
      match !warnings with
      | [] -> `Assoc [ ("status", `String "OK"); ("data", data) ]
      | l -> `Assoc [ ("status", `String "WARNING"); ("messages", `List l); ("data", data) ]
    with
    | Error msg -> `Assoc [ ("status", `String "ERROR"); ("message", `String msg) ]
    | Conll_error t -> `Assoc [ ("status", `String "ERROR"); ("message", t) ]
    | Grewlib.Error t -> `Assoc [ ("status", `String "ERROR"); ("message", `String t) ]
    | Amr.Error t -> `Assoc [ ("status", `String "ERROR"); ("message", `String t) ]
    | exc -> `Assoc [ ("status", `String "UNEXPECTED_EXCEPTION"); ("exception", `String (Printexc.to_string exc)) ] in
  json

(* ================================================================================ *)
(* global variables *)
(* ================================================================================ *)
let (global : string String_map.t ref) = ref String_map.empty
let set_global key value = global := String_map.add key value !global
let get_global key =
  try String_map.find key !global
  with Not_found -> raise (Error (sprintf "Config error: global parameter `%s` is not set" key))

(* ================================================================================ *)
(* read_config *)
(* ================================================================================ *)
let read_config () =
  try
    let elements =
      List.map
        (fun item ->
           Ocsigen_extensions.Configuration.element
             ~name: item
             ~pcdata: (fun x -> printf " INFO:  ---> set `%s` config parameter to `%s`\n%!" item x; set_global item x)
             ()
        ) ["log"; "extern"; "base_url"] in

    Ocsigen_extensions.Configuration.process_elements
      ~in_tag:"eliommodule"
      ~elements
      (Eliom_config.get_config ())
  with
  | Error msg -> printf " ERROR: ================ Starting error: %s ================\n%!" msg; exit 0

(* ================================================================================ *)
(* Log *)
(* ================================================================================ *)
module Log = struct
  let out_ch = ref stdout

  let time_stamp () =
    let gm = Unix.localtime (Unix.time ()) in
    Printf.sprintf "%02d_%02d_%02d_%02d_%02d_%02d"
      (gm.Unix.tm_year - 100)
      (gm.Unix.tm_mon + 1)
      gm.Unix.tm_mday
      gm.Unix.tm_hour
      gm.Unix.tm_min
      gm.Unix.tm_sec

  let init () =
    let basename = Printf.sprintf "draw_server_%s.log" (time_stamp ()) in
    let filename = Filename.concat (get_global "log") basename in
    out_ch := open_out filename

  let _info s = Printf.fprintf !out_ch "[%s] %s\n%!" (time_stamp ()) s
  let info s = Printf.ksprintf _info s
end

(* ================================================================================ *)
(* main *)
(* ================================================================================ *)

let _ = read_config ()

let images_dir = get_global "extern"
let images_url = get_global "base_url"

let _ = Log.init ()
